CC = cc
CFLAGS = -ansi -Wall -Wextra -Werror

test:
	$(CC) $(CFLAGS) -o astr tests/astr.c src/astr.c -g
	./astr
	$(CC) $(CFLAGS) -o ustr tests/ustr.c src/ustr.c -g
	./ustr
clean:
	rm -f astr ustr

