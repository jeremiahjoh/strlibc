#include "../src/ustr.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void
construct()
{
	char *str, *sli;

	str = ustr_new("안녕! 세상아.");

	assert(str != NULL);
	assert(strcmp(str, "안녕! 세상아.") == 0);

	sli = ustr_slice(str, 4, 7);
	assert(strcmp(sli, "세상아") == 0);

	str = ustr_from(0x20AC);
	assert(strcmp(str, "€") == 0);
}

void
clear()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_clear(&str);

	assert(strlen(str) == 0);
	assert(str[0] == '\0');
}

void
push()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_push_back_str(&str, " 난 사람이야.");
	ustr_push_front_str(&str, "아름다운 ");
	ustr_push_back_chr(&str, '!');
	ustr_push_front_chr(&str, '!');

	assert(strcmp(str, "!아름다운 안녕! 세상아. 난 사람이야.!") == 0);
}

void
insert()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_insert_str(&str, "아름다운 ", 4);
	ustr_insert_chr(&str, '!', 8);

	assert(strcmp(str, "안녕! 아름다운! 세상아.") == 0);
}

void
search()
{
	char *str;
	size_t idx;

	str = ustr_new("안녕! 세상아.");

	ustr_search_str(str, "세상", &idx);
	assert(idx == 4);

	ustr_search_chr(str, '!', &idx);
	assert(idx == 2);
}

void
delete()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_remove_str(&str, " 세상아.");
	assert(strcmp(str, "안녕!") == 0);

	ustr_remove_chr(&str, '!');
	assert(strcmp(str, "안녕") == 0);
}

void
nth()
{
	char *str;
	size_t chr;

	str = ustr_new("안녕! 세상아.");

	ustr_getnth(str, &chr, 4);
	assert(chr == 0xC138);

	ustr_rmvnth(&str, &chr, 4);
	assert(chr == 0xC138);

	ustr_setnth(&str, chr, 4);
	assert(strcmp(str, "안녕! 세아.") == 0);
}

void
valid()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	assert(ustr_is_valid(str));

	str[3] = 0x01;
	assert(!ustr_is_valid(str));

	str[0] = 0xF0;
	str[1] = 0x82;
	str[2] = 0x82;
	str[3] = 0xAC;
	str[4] = 0x00;
	assert(!ustr_is_valid(str));
}

void
size()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	assert(ustr_length(str) == 8);
	assert(ustr_sizeof(str) == 32);
}

void
file()
{
	char *str;

	str = ustr_line(stdin);

	puts(str);
}

void
assign()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_assign(&str, "Goodbye!");

	assert(strcmp(str, "Goodbye!") == 0);
}

void
assign_at()
{
	char *str;

	str = ustr_new("안녕! 세상아.");

	ustr_assign_at(&str, "만나서 반갑다.", 4);

	assert(strcmp(str, "안녕! 만나서 반갑다.") == 0);
}

int
main()
{
	construct();
	clear();
	push();
	insert();
	search();
	delete();
	nth();
	valid();
	size();
	file();
	assign();
	assign_at();

	return 0;
}
