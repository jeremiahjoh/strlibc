#include "../src/astr.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void
construct()
{
	char *str;

	str = astr_new("Hello, world!");
	assert(strcmp(str, "Hello, world!") == 0);
	astr_free(&str);

	str = astr_from('!');
	assert(strcmp(str, "!") == 0);
	astr_free(&str);

	str = astr_empty();
	assert(strlen(str) == 0);
	astr_free(&str);
}

void
clear()
{
	char *str;

	str = astr_new("안녕! 세상아.");

	astr_clear(&str);

	assert(strlen(str) == 0);
	assert(str[0] == '\0');
}

void
slice()
{
	char *sli;

	sli = astr_slice("Hello, world!", 7, 13);
	assert(strcmp(sli, "world!") == 0);
}

void
push_chr()
{
	char *str;
	int i;

	str = astr_new("Hello, world");

	for (i = 0; i < 8; i++)
		astr_push_back_chr(&str, '!');
	assert(strcmp(str, "Hello, world!!!!!!!!") == 0);

	for (i = 0; i < 8; i++)
		astr_push_front_chr(&str, '!');
	assert(strcmp(str, "!!!!!!!!Hello, world!!!!!!!!") == 0);

	astr_free(&str);
}

void
push_str()
{
	char *str;

	str = astr_new("Hello, world!");

	astr_push_back_str(&str, " I love you!");
	assert(strcmp(str, "Hello, world! I love you!") == 0);

	astr_push_front_str(&str, "Hey! ");
	assert(strcmp(str, "Hey! Hello, world! I love you!") == 0);

	astr_free(&str);
}

void
insert()
{
	char *str;

	str = astr_new("Hello world!");

	astr_insert_chr(&str, ',', 5);
	assert(strcmp(str, "Hello, world!") == 0);

	astr_insert_str(&str, "beautiful ", 7);
	assert(strcmp(str, "Hello, beautiful world!") == 0);

	astr_free(&str);
}

void
search()
{
	char *str;
	size_t i;

	str = astr_new("Hello, world!");

	astr_search_chr(str, ',', &i);
	assert(i == 5);

	astr_search_str(str, "world!", &i);
	assert(i == 7);

	astr_free(&str);
}

void
delete()
{
	char *str;

	str = astr_new("Hello, world!");

	astr_remove_chr(&str, ',');
	assert(strcmp(str, "Hello world!") == 0);

	astr_remove_str(&str, " world");
	assert(strcmp(str, "Hello!") == 0);

	astr_free(&str);
}

void
nth()
{
	char *str;
	char chr;

	str = astr_new("Hello, world!");

	astr_getnth(str, &chr, 5);
	assert(chr == ',');

	astr_setnth(&str, '!', 5);
	assert(strcmp(str, "Hello! world!") == 0);

	astr_rmvnth(&str, &chr, 5);
	assert(strcmp(str, "Hello world!") == 0);

	astr_free(&str);
}

void
valid()
{
	char *str;

	str = astr_new("Hello, world!");
	assert(astr_is_valid(str));

	str[8] = 0x80; /* which is invalid in ASCII */
	assert(!astr_is_valid(str));
}

void
size()
{
	char *str;

	str = astr_new("Hello, world!");

	assert(astr_length(str) == 13);
	assert(astr_sizeof(str) == 16);
}

void
file()
{
	char *str;

	str = astr_line(stdin);

	puts(str);
}

void
assign()
{
	char *str;

	str = astr_new("Hello, world!");

	astr_assign(&str, "Goodbye!");

	assert(strcmp(str, "Goodbye!") == 0);
}

void
assign_at()
{
	char *str;

	str = astr_new("Hello, world!");

	astr_assign_at(&str, "my friend!", 7);

	assert(strcmp(str, "Hello, my friend!") == 0);
}

int
main()
{
	construct();
	clear();
	slice();
	push_chr();
	push_str();
	search();
	insert();
	delete();
	valid();
	size();
	file();
	assign();
	assign_at();

	return 0;
}
