/*
 * strlibc - dynamic string library written in ANSI C
 *
 * Written in 2024 by Woohyun Joh <jeremiahjoh@sungkyul.ac.kr>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software.
 */

#ifndef _USTR_H
#define _USTR_H

#include <stdio.h>
#include <stddef.h>

char *ustr_new(const char *);
char *ustr_empty();
char *ustr_from(const size_t);
char *ustr_slice(const char *, const size_t, const size_t);
char *ustr_until(FILE *, const size_t);
char *ustr_line(FILE *);
char *ustr_all(FILE *);
int ustr_clear(char **);
int ustr_push_back_chr(char **, const size_t);
int ustr_push_back_str(char **, const char *);
int ustr_push_front_chr(char **, const size_t);
int ustr_push_front_str(char **, const char *);
int ustr_insert_chr(char **, const size_t, const size_t);
int ustr_insert_str(char **, const char *, const size_t);
int ustr_remove_chr(char **, const size_t);
int ustr_remove_str(char **, const char *);
int ustr_search_chr(const char *, const size_t, size_t *);
int ustr_search_str(const char *, const char *, size_t *);
int ustr_getnth(const char *, size_t *, const size_t);
int ustr_rmvnth(char **, size_t *, const size_t);
int ustr_setnth(char **, const size_t, const size_t);
int ustr_assign(char **, const char *);
int ustr_assign_at(char **, const char *, const size_t);
int ustr_is_valid(const char *);
size_t ustr_length(const char *);
size_t ustr_sizeof(const char *);
void ustr_free(char **);

#endif
