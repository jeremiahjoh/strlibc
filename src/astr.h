/*
 * strlibc - dynamic string library written in ANSI C
 *
 * Written in 2024 by Woohyun Joh <jeremiahjoh@sungkyul.ac.kr>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software.
 */

#ifndef _ASTR_H
#define _ASTR_H

#include <stdio.h>
#include <stddef.h>

char *astr_new(const char *);
char *astr_empty();
char *astr_from(const char);
char *astr_slice(const char *, const size_t, const size_t);
char *astr_until(FILE *, const int);
char *astr_line(FILE *);
char *astr_all(FILE *);
int astr_clear(char **);
int astr_push_back_chr(char **, const char);
int astr_push_back_str(char **, const char *);
int astr_push_front_chr(char **, const char);
int astr_push_front_str(char **, const char *);
int astr_insert_chr(char **, const char, const size_t);
int astr_insert_str(char **, const char *, const size_t);
int astr_remove_chr(char **, const char);
int astr_remove_str(char **, const char *);
int astr_search_chr(const char *, const char, size_t *);
int astr_search_str(const char *, const char *, size_t *);
int astr_getnth(const char *, char *, const size_t);
int astr_rmvnth(char **, char *, const size_t);
int astr_setnth(char **, const char, const size_t);
int astr_assign(char **, const char *);
int astr_assign_at(char **, const char *, const size_t);
int astr_is_valid(const char *);
size_t astr_length(const char *);
size_t astr_sizeof(const char *);
void astr_free(char **);

#endif
