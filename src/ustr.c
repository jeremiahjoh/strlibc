/*
 * strlibc - dynamic string library written in ANSI C
 *
 * Written in 2024 by Woohyun Joh <jeremiahjoh@sungkyul.ac.kr>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software.
 */

#include "ustr.h"
#include <stdlib.h>
#include <string.h>
#define BETWEEN(x, y, z) ((x) < (y) && (y) < (z))

static size_t
codepoint(const char utf8[5])
{
	size_t chr = 0;

	if ((unsigned char)*utf8 < 0x80) {
		chr = *utf8;
	} else if ((unsigned char)*utf8 < 0xE0) {
		chr |= ((size_t)utf8[0] & 0x3F) << 6;
		chr |= ((size_t)utf8[1] & 0x3F);
	} else if ((unsigned char)*utf8 < 0xF0) {
		chr |= ((size_t)utf8[0] & 0x0F) << 12;
		chr |= ((size_t)utf8[1] & 0x3F) << 6;
		chr |= ((size_t)utf8[2] & 0x3F);
	} else if ((unsigned char)*utf8 < 0xF8) {
		chr |= ((size_t)utf8[0] & 0x07) << 18;
		chr |= ((size_t)utf8[1] & 0x3F) << 12;
		chr |= ((size_t)utf8[2] & 0x3F) << 6;
		chr |= ((size_t)utf8[3] & 0x3F);
	} else {
		return -1;
	}

	return chr;
}

static size_t
utf8_len(char c)
{
	if ((unsigned char)c < 0x80)
		return 1;
	if ((unsigned char)c < 0xE0)
		return 2;
	if ((unsigned char)c < 0xF0)
		return 3;
	if ((unsigned char)c < 0xF8)
		return 4;
	/* UNREACHABLE */
	return 0;
}

static int
chr2utf8(char utf8[5], size_t chr)
{
	memset(utf8, '\0', 5);
	if (chr < 0x7F) {
		utf8[0] = chr;
	} else if (chr < 0x07FF) {
		utf8[1] = (chr       & 0x3F) | 0x80;
		utf8[0] = (chr >> 6          | 0xC0);
	} else if (chr < 0xFFFF) {
		utf8[2] = (chr       & 0x3F) | 0x80;
		utf8[1] = (chr >> 6  & 0x3F) | 0x80;
		utf8[0] = (chr >> 12         | 0xE0);
	} else if (chr < 0x10FFFF) {
		utf8[3] = (chr       & 0x3F) | 0x80;
		utf8[2] = (chr >> 6  & 0x3F) | 0x80;
		utf8[1] = (chr >> 12 & 0x3F) | 0x80;
		utf8[0] = (chr >> 18         | 0xF0);
	} else {
		return -1;
	}

	return 0;
}

static size_t
index(const char *str, const size_t n)
{
	size_t i, idx;

	for (i = idx = 0; i < n && str[idx] != '\0'; i++)
		idx += utf8_len(str[idx]);

	return idx;
}

static size_t
capacity(size_t len)
{
	size_t cap;

	for (cap = 1; cap <= len; cap <<= 1)
		;

	return cap;
}

static int
resize(char **str, const size_t len)
{
	size_t new_cap;

	new_cap = capacity(len);

	if (capacity(strlen(*str)) == new_cap)
		return 0;
	if ((*str = realloc(*str, new_cap)) == NULL)
		return -1;

	memset(*str + len, '\0', new_cap - len);

	return 0;
}

char *
ustr_new(const char *raw)
{
	char *str;

	if (!ustr_is_valid(raw))
		return NULL;

	str = calloc(capacity(strlen(raw)), sizeof(char));
	strcpy(str, raw);

	return str;
}

char *
ustr_empty()
{
	return calloc(1, sizeof(char));
}

char *
ustr_from(size_t chr)
{
	char *str;
	char utf8[5];

	if (chr2utf8(utf8, chr))
		return NULL;
	if ((str = calloc(capacity(strlen(utf8)), sizeof(char))) == NULL)
		return NULL;

	strcpy(str, utf8);

	return str;
}

char *
ustr_slice(const char *str, const size_t head, const size_t tail)
{
	char *sli;
	size_t head_i, tail_i, len;

	if (tail <= head)
		return ustr_empty();

	head_i = index(str, head);
	tail_i = index(str, tail);
	len = tail_i - head_i;

	sli = calloc(capacity(len + 1), sizeof(char));
	memcpy(sli, str + head_i, len);

	return sli;
}

char *
ustr_until(FILE *fp, const size_t delim)
{
	char *str;
	size_t chr;
	size_t cap;

	cap = 0;
	str = ustr_empty();

	while (!feof(fp) && !ferror(fp)) {
		if ((chr = fgetc(fp)) == delim)
			return str;
		if (resize(&str, ++cap))
			break;

		str[cap - 1] = chr;
	}

	free(str);

	return NULL;
}

char *
ustr_line(FILE *fp)
{
	return ustr_until(fp, '\n');
}

char *
ustr_all(FILE *fp)
{
	return ustr_until(fp, EOF);
}

int
ustr_clear(char **str)
{
	if ((*str = realloc(*str, 1)) == NULL)
		return -1;

	**str = '\0';

	return 0;
}

int
ustr_push_back_chr(char **str, size_t chr)
{
	char utf8[5];
	size_t len;

	if (chr2utf8(utf8, chr))
		return -1;

	len = strlen(*str);
	if (resize(str, len + strlen(utf8) + 1))
		return -1;

	strcat(*str, utf8);

	return 0;
}

int
ustr_push_back_str(char **str, const char *sub)
{
	if (!ustr_is_valid(sub))
		return -1;
	if (resize(str, strlen(*str) + strlen(sub) + 1))
		return -1;

	strcat(*str, sub);

	return 0;
}

int
ustr_push_front_chr(char **str, size_t chr)
{
	char utf8[5];
	size_t str_len, chr_len;

	if (chr2utf8(utf8, chr))
		return -1;

	str_len = strlen(*str);
	chr_len = strlen(utf8);
	if (resize(str, str_len + chr_len + 1))
		return -1;

	memmove(*str + chr_len, *str, str_len);
	memcpy(*str, utf8, chr_len);

	return 0;
}

int
ustr_push_front_str(char **str, const char *sub)
{
	size_t str_len, sub_len;

	if (!ustr_is_valid(sub))
		return -1;

	str_len = strlen(*str);
	sub_len = strlen(sub);
	if (resize(str, str_len + sub_len + 1))
		return -1;

	memmove(*str + sub_len, *str, str_len);
	memcpy(*str, sub, sub_len);

	return 0;
}

int
ustr_insert_chr(char **str, const size_t chr, const size_t idx)
{
	char utf8[5];
	size_t str_len, chr_len, utf_idx;

	if (chr2utf8(utf8, chr))
		return -1;

	str_len = strlen(*str);
	chr_len = strlen(utf8);
	if (resize(str, str_len + chr_len + 1))
		return -1;

	utf_idx = index(*str, idx);
	memmove(*str + utf_idx + chr_len, *str + utf_idx, str_len - utf_idx);
	memcpy(*str + utf_idx, utf8, chr_len);

	return 0;
}

int
ustr_insert_str(char **str, const char *sub, const size_t idx)
{
	size_t str_len, sub_len, utf_idx;

	if (!ustr_is_valid(sub))
		return -1;

	str_len = strlen(*str);
	sub_len = strlen(sub);
	if (resize(str, str_len + sub_len + 1))
		return -1;

	utf_idx = index(*str, idx);
	memmove(*str + utf_idx + sub_len, *str + utf_idx, str_len - utf_idx);
	memcpy(*str + utf_idx, sub, sub_len);

	return 0;
}

int
ustr_search_chr(const char *str, const size_t chr, size_t *idx)
{
	char utf8[5];
	size_t i, chr_len;

	if (chr2utf8(utf8, chr))
		return -1;

	chr_len = strlen(utf8);
	for (i = *idx = 0; str[i] != '\0'; i += utf8_len(str[i]), (*idx)++)
		if (strncmp(utf8, str + i, chr_len) == 0)
			return 0;

	return -1;
}

int
ustr_search_str(const char *str, const char *sub, size_t *idx)
{
	size_t i, sub_len;

	if (!ustr_is_valid(sub))
		return -1;

	sub_len = strlen(sub);
	for (i = *idx = 0; str[i] != '\0'; i += utf8_len(str[i]), (*idx)++)
		if (strncmp(sub, str + i, sub_len) == 0)
			return 0;

	return -1;
}

int
ustr_remove_chr(char **str, const size_t chr)
{
	char utf8[5], *tar;
	size_t i, str_len, chr_len;

	if (chr2utf8(utf8, chr))
		return -1;

	str_len = strlen(*str);
	chr_len = strlen(utf8);
	if ((tar = strstr(*str, utf8)) == NULL)
		return -1;

	i = tar - *str;
	memmove(tar, tar + chr_len, str_len - i);

	return resize(str, str_len - chr_len + 1);
}

int
ustr_remove_str(char **str, const char *sub)
{
	char *tar;
	size_t i, str_len, chr_len;

	if (!ustr_is_valid(sub))
		return -1;

	str_len = strlen(*str);
	chr_len = strlen(sub);
	if ((tar = strstr(*str, sub)) == NULL)
		return -1;

	i = tar - *str;
	memmove(tar, tar + chr_len, str_len - i);

	return resize(str, str_len - chr_len + 1);
}

int
ustr_getnth(const char *str, size_t *chr, const size_t idx)
{
	const char *utf8;
	size_t utf_idx;

	*chr = 0;
	utf_idx = index(str, idx);
	utf8 = str + utf_idx;

	*chr = codepoint(utf8);

	return 0;
}

int
ustr_setnth(char **str, const size_t chr, const size_t idx)
{
	size_t buf;

	if (ustr_rmvnth(str, &buf, idx))
		return -1;
	if (ustr_insert_chr(str, chr, idx))
		return -1;

	return 0;
}

int
ustr_rmvnth(char **str, size_t *chr, const size_t idx)
{
	size_t utf_idx, str_len, chr_len;

	str_len = strlen(*str);
	utf_idx = index(*str, idx);
	chr_len = utf8_len((*str)[utf_idx]);

	*chr = codepoint(*str + utf_idx);

	memmove(*str + utf_idx,
	        *str + utf_idx + chr_len,
	        str_len - utf_idx + chr_len);

	return resize(str, str_len - chr_len + 1);
}

int
ustr_assign(char **old, const char *new)
{
	if (resize(old, strlen(new)))
		return -1;

	strcpy(*old, new);

	return 0;
}

int
ustr_assign_at(char **str, const char *sub, const size_t idx)
{
	size_t str_len, sub_len, str_idx;

	str_len = strlen(*str);
	sub_len = strlen(sub);
	str_idx = index(*str, idx);

	if (str_idx > str_len)
		return -1;
	if (resize(str, str_idx + sub_len))
		return -1;

	strcpy(*str + str_idx, sub);

	return 0;
}

int
ustr_is_valid(const char *str)
{
	size_t i, o;

	for (i = 0; str[i] != '\0'; i += o) {
		o = utf8_len(str[i]);
		if (BETWEEN(0x7F, (unsigned char)str[i + o], 0xC0))
			return 0;
		if (o == 1)
			return (str[i] & 0x80) ? 0 : 1;
		if ((unsigned char)(str[i] << o) == 0)
			return 0;
	}

	return 1;
}

size_t
ustr_length(const char *str)
{
	size_t i, len;

	for (i = len = 0; str[i] != '\0'; i += utf8_len(str[i]), len++)
		;

	return len;
}

size_t
ustr_sizeof(const char *str)
{
	return capacity(strlen(str));
}

void
ustr_free(char **str)
{
	free(*str);
	*str = NULL;
}

