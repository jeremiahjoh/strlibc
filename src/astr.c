/*
 * strlibc - dynamic string library written in ANSI C
 *
 * Written in 2024 by Woohyun Joh <jeremiahjoh@sungkyul.ac.kr>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software.
 */

#include "astr.h"
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

static size_t
capacity(const size_t len)
{
	size_t cap;

	for (cap = 1; cap <= len; cap <<= 1)
		;

	return cap;
}

static int
resize(char **str, const size_t len)
{
	size_t new_cap;

	new_cap = capacity(len);

	if (capacity(strlen(*str)) == new_cap)
		return 0;
	if ((*str = realloc(*str, new_cap)) == NULL)
		return -1;

	memset(*str + len, '\0', new_cap - len);

	return 0;
}

char *
astr_new(const char *raw_str)
{
	char *str;

	str = calloc(capacity(strlen(raw_str)), sizeof(char));
	strcpy(str, raw_str);

	return str;
}

char *
astr_empty()
{
	return calloc(1, sizeof(char));
}

char *
astr_from(const char chr)
{
	char *str;

	str = calloc(2, sizeof(char));
	str[0] = chr;

	return str;
}

char *
astr_slice(const char *str, const size_t head, const size_t tail)
{
	char *sli;
	size_t str_len;

	if (head >= tail || tail > strlen(str))
		return astr_empty();

	str_len = tail - head;

	sli = calloc(capacity(str_len), sizeof(char));
	strncpy(sli, str + head, str_len);

	return sli;
}

char *
astr_until(FILE *fp, const int delim)
{
	char *str;
	int chr;

	str = astr_empty();

	while (!feof(fp) && !ferror(fp)) {
		if ((chr = fgetc(fp)) == delim)
			return str;
		if (astr_push_back_chr(&str, chr))
			break;
	}

	free(str);

	return NULL;
}

char *
astr_line(FILE *fp)
{
	return astr_until(fp, '\n');
}

char *
astr_all(FILE *fp)
{
	return astr_until(fp, EOF);
}

int
astr_clear(char **str)
{
	if ((*str = realloc(*str, 1)) == NULL)
		return -1;

	**str = '\0';

	return 0;
}

int
astr_push_back_chr(char **str, const char chr)
{
	size_t str_len;

	str_len = strlen(*str);

	if (resize(str, str_len + 1))
		return -1;

	(*str)[str_len] = chr;

	return 0;
}

int
astr_push_back_str(char **str, const char *sub)
{
	if (resize(str, strlen(*str) + strlen(sub)))
		return -1;

	strcat(*str, sub);

	return 0;
}

int
astr_push_front_chr(char **str, const char chr)
{
	size_t str_len;

	str_len = strlen(*str);

	if (resize(str, str_len + 1))
		return -1;

	memmove(*str + 1, *str, str_len);
	**str = chr;

	return 0;
}

int
astr_push_front_str(char **str, const char *sub)
{
	size_t str_len, sub_len;

	str_len = strlen(*str);
	sub_len = strlen(sub);

	if (resize(str, str_len + sub_len))
		return -1;

	memmove(*str + sub_len, *str, str_len);
	memcpy(*str, sub, sub_len);

	return 0;
}

int
astr_insert_chr(char **str, const char chr, const size_t idx)
{
	size_t str_len;

	str_len = strlen(*str);

	if (resize(str, str_len + 1))
		return -1;

	memmove(*str + idx + 1, *str + idx, str_len - idx);
	(*str)[idx] = chr;

	return 0;
}

int
astr_insert_str(char **str, const char *sub, const size_t idx)
{
	size_t str_len, sub_len;

	str_len = strlen(*str);
	sub_len = strlen(sub);

	if (resize(str, str_len + sub_len))
		return -1;

	memmove(*str + idx + sub_len, *str + idx, str_len - idx);
	memcpy(*str + idx, sub, sub_len);

	return 0;
}

int
astr_remove_chr(char **str, const char chr)
{
	size_t str_len;
	char *loc;

	if ((loc = strchr(*str, chr)) == NULL)
		return -1;

	memmove(loc, loc + 1, strlen(loc));
	str_len = strlen(*str);

	return resize(str, str_len - 1);
}

int
astr_remove_str(char **str, const char *sub)
{
	size_t str_len, sub_len;
	char *loc;

	if ((loc = strstr(*str, sub)) == NULL)
		return -1;

	str_len = strlen(*str);
	sub_len = strlen(sub);

	memmove(loc, loc + sub_len, str_len - sub_len - (loc - *str));

	return resize(str, strlen(*str) - sub_len);
}

int
astr_search_chr(const char *str, const char chr, size_t *idx)
{
	char *loc;

	if ((loc = strchr(str, chr)) == NULL)
		return -1;

	*idx = (size_t)(loc - str);

	return 0;
}

int
astr_search_str(const char *str, const char *sub, size_t *idx)
{
	char *loc;

	if ((loc = strstr(str, sub)) == NULL)
		return -1;

	*idx = (size_t)(loc - str);

	return 0;
}

int
astr_getnth(const char *str, char *chr, const size_t idx)
{
	if (idx >= strlen(str))
		return -1;

	*chr = str[idx];

	return 0;
}

int
astr_setnth(char **str, const char chr, const size_t idx)
{
	if (idx >= strlen(*str))
		return -1;

	(*str)[idx] = chr;

	return 0;
}

int
astr_rmvnth(char **str, char *chr, const size_t idx)
{
	if (idx >= strlen(*str))
		return -1;

	*chr = (*str)[idx];

	memmove(*str + idx, *str + idx + 1, strlen(*str) - idx);

	return 0;
}

int
astr_assign(char **old, const char *new)
{
	if (resize(old, strlen(new)))
		return -1;

	strcpy(*old, new);

	return 0;
}

int
astr_assign_at(char **str, const char *sub, const size_t idx)
{
	size_t str_len;
	size_t sub_len;

	str_len = strlen(*str);
	sub_len = strlen(sub);

	if (idx > str_len)
		return -1;
	if (resize(str, idx + sub_len))
		return -1;

	strcpy(*str + idx, sub);

	return 0;
}

int
astr_is_valid(const char *str)
{
	for (; *str; str++)
		if (*str & 0x80)
			return 0;

	return 1;
}

size_t
astr_length(const char *str)
{
	return strlen(str);
}

size_t
astr_sizeof(const char *str)
{
	return capacity(strlen(str));
}

void
astr_free(char **str)
{
	free(*str);
	*str = NULL;
}
