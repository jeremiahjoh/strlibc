String Library for C
====================

Dynamic-length string library for ANSI C.

| header file | string type | character type | description            |
|:-----------:|-------------|----------------|------------------------|
| astr.h      | astr        | char           | ASCII String           |
| ustr.h      | ustr        | uchr           | Unicode String (UTF-8) |

Since `astr` and `ustr` have identical function interfaces,
you can replace `<str>` and `<chr>` depends on what you need.

`<str> <str>_new(<chr> *chr)`
-----------------------------

Takes null-terminated string and returns constructed string object.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello, world!");

assert(strcmp(str, "Hello, world!") == 0);
```

`<str> <str>_empty()`
---------------------

Returns empty string terminated with `\0`.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_empty();

assert(strcmp(str, "") == 0);
```

`<str> <str>_from(<chr> chr)`
-----------------------------

Takes a character and returns string with given character.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_from('A');

assert(strcmp(str, "A") == 0);
```

`<str> <str>_slice(const <str> str, const size_t head, const size_t tail)`
--------------------------------------------------------------------------

Takes original string, beginning point and ending point.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello, world!");
astr sli = astr_slice(str, 7, 12);

assert(strcmp(sli, "world!") == 0);
```

`int <str>_push_back_chr(<str> *str, const <chr> chr)`
------------------------------------------------------

Places character `chr` at the end of string `str`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello, world");
astr_push_back_chr(&str, `!`);

assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_push_back_str(<str> *str, const <str> sub)`
------------------------------------------------------

Places string `sub` at the end of string `str`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello");
astr_push_back_str(&str, ", world!");

assert(strcmp(str, "Hello, world!") == 0);
```

or

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello");
astr sub = astr_new(", world!");
astr_push_back_str(&str, sub);

assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_push_front_chr(<str> *str, const <chr> chr)`
------------------------------------------------------

Places character `chr` at the beginning of string `str`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("ello, world!");
astr_push_front_chr(&str, 'H');

assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_push_front_str(<str> *str, const <str> str)`
------------------------------------------------------

Places string `sub` at the beginning of string `str`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("world!");
astr_push_front_str(&str, "Hello, ");

assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_insert_chr(<str> *str, const <chr> chr, const size_t idx)`
---------------------------------------------------------------------

Inserts character `chr` at given index `idx`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello world!");
astr_insert_chr(&str, ',', 5);

assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_insert_str(<str> *str, const <str> sub, const size_t idx)`
---------------------------------------------------------------------

Inserts string `sub` at given index `idx`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Helloworld!");
astr_insert_str(&str, ", ", 5);

assert(strcmp(str, "Hello, world!") == 0);
```

`size_t <str>_remove_chr(<str> *str, const <chr> chr)`
------------------------------------------------------

Finds target character `chr` in string `str` and removes it. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello!, world!");
size_t loc = astr_remove_chr(&str, '!');

assert(strcmp(str, "Hello, world!") == 0);
assert(loc == 5);
```

`size_t <str>_remove_str(<str> *str, const <str> sub)`
------------------------------------------------------

Finds target string `sub` in string `str` and removes it. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello, third world!");
size_t loc = astr_remove_str(&str, "third");

assert(strcmp(str, "Hello, world!") == 0);
assert(loc == 7);
```

`size_t <str>_search_chr(<str> *str, const <chr> chr)`
------------------------------------------------------

Finds target character `chr` in string `str`.
Returns location of target character, length of string on failure.

```c
#include "astr.h"
#include <assert.h>

astr str = astr_new("Hello, world!");
size_t loc = astr_search_chr(&str, ',');

assert(loc == 5);
```

`size_t <str>_search_str(<str> *str, const <str> sub)`
------------------------------------------------------

Finds target string `sub` in string `str`.
Returns location of target character, length of string on failure.

```c
#include "astr.h"
#include <assert.h>

astr str = astr_new("Hello, world!");
size_t loc = astr_search_chr(&str, "world!");

assert(loc == 7);
```

`int <str>_getnth(const <str> *str, <chr> *chr, const size_t idx)`
------------------------------------------------------------------

Assigns character at given index `idx` in string `str' to 'chr'. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>

char chr;
astr str = astr_new("Hello, world!");
astr_getnth(&str, &chr, 5);

assert(chr == ',');
```

`int <str>_rmvnth(<str> *str, <chr> *chr, const size_t idx)`
------------------------------------------------------------

Removes character at given index `idx` in string `str` into `chr`. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

char chr;
astr str = astr_new("Hello!, world!");
astr_rmvnth(&str, &chr, 5);

assert(chr == '!');
assert(strcmp(str, "Hello, world!") == 0);
```

`int <str>_setnth(<str> *str, const <chr> chr, const size_t idx)`
-----------------------------------------------------------------

Assigns character `chr` at given index `idx` in string `str'. 
Returns 0 on success, -1 on failure.

```c
#include "astr.h"
#include <assert.h>
#include <string.h>

astr str = astr_new("Hello! world!");
astr_setnth(&str, ',', 5);

assert(strcmp(str, "Hello, world!") == 0);
```

`size_t <str>_length(const <str> str)`
--------------------------------------

Returns number of characters in string `str`.

```c
#include "astr.h"
#include <assert.h>

astr str = astr_new("Hello, world!");
size_t len = astr_length(str);

assert(len == 12);
```

`size_t <str>_sizeof(const <str> str)`
--------------------------------------

Returns how many bytes string `str` is occupying.

```c
#include "astr.h"
#include <assert.h>

astr str = astr_new("Hello, world!");
size_t len = astr_sizeof(str);

assert(len == 13);
```

`void <str>_free(<str> *str)`
-----------------------------

Deallocate string `str` from memory and assign NULL.

```c
#include "astr.h"
#include <assert.h>

astr str = astr_new("Hello, world!");
astr_free(&str);

assert(str == NULL);
```
